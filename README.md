# MPU_9250_STM32

Rewriting library for IMU module MPU9250 made using Mbed OS in order to enable use it with HAL libraries on STM32L432KC MCU. Library implements opensource Madgwick filter which is use to filter euler angles and will be used in order to make compass. The newest version is on branch "c_file".
