/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "MPU9250.h"
#include <stdio.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
uint8_t test_buf[50]={0};  // added by me for debugging by uart
float sum = 0;
uint32_t sumCount = 0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C1_Init(void);
static void MX_USART2_UART_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */

  // Read the WHO_AM_I register, this is a good test of communication
    uint8_t whoami = readByte(MPU9250_ADDRESS, WHO_AM_I_MPU9250);  // Read WHO_AM_I register for MPU-9250
   // pc.printf("I AM 0x%x\n\r", whoami); pc.printf("I SHOULD BE 0x71\n\r");

    if (whoami == 0x71) // WHO_AM_I should always be 0x68
    {
    //  pc.printf("MPU9250 is online...\n\r");
     // wait(1);
 	   HAL_Delay(1000);
      //lcd.clear();
      //lcd.printString("MPU9250 OK", 0, 0);

      resetMPU9250(); // Reset registers to default in preparation for device calibration
      calibrateMPU9250(gyroBias, accelBias); // Calibrate gyro and accelerometers, load biases in bias registers
   //   pc.printf("x gyro bias = %f\n\r", gyroBias[0]);
    //  pc.printf("y gyro bias = %f\n\r", gyroBias[1]);
    //  pc.printf("z gyro bias = %f\n\r", gyroBias[2]);
    //  pc.printf("x accel bias = %f\n\r", accelBias[0]);
    //  pc.printf("y accel bias = %f\n\r", accelBias[1]);
    //  pc.printf("z accel bias = %f\n\r", accelBias[2]);
      //wait(2);
      HAL_Delay(2000);
      initMPU9250();
    //  pc.printf("MPU9250 initialized for active data mode....\n\r"); // Initialize device for active mode read of acclerometer, gyroscope, and temperature
      initAK8963(magCalibration);
    //  pc.printf("AK8963 initialized for active data mode....\n\r"); // Initialize device for active mode read of magnetometer
    //  pc.printf("Accelerometer full-scale range = %f  g\n\r", 2.0f*(float)(1<<Ascale));
    //  pc.printf("Gyroscope full-scale range = %f  deg/s\n\r", 250.0f*(float)(1<<Gscale));
    //  if(Mscale == 0) pc.printf("Magnetometer resolution = 14  bits\n\r");
  //    if(Mscale == 1) pc.printf("Magnetometer resolution = 16  bits\n\r");
  //    if(Mmode == 2) pc.printf("Magnetometer ODR = 8 Hz\n\r");
   //   if(Mmode == 6) pc.printf("Magnetometer ODR = 100 Hz\n\r");
   //   wait(2);
      HAL_Delay(2000);
     }
     else
     {
   //   pc.printf("Could not connect to MPU9250: \n\r");
   //   pc.printf("%#x \n",  whoami);

   //   lcd.clear();
    //  lcd.printString("MPU9250", 0, 0);
    //  lcd.printString("no connection", 0, 1);
   //   lcd.printString("0x", 0, 2);  lcd.setXYAddress(20, 2); lcd.printChar(whoami);

      while(1) ; // Loop forever if communication doesn't happen
      }

      getAres(); // Get accelerometer sensitivity
      getGres(); // Get gyro sensitivity
      getMres(); // Get magnetometer sensitivity
      //pc.printf("Accelerometer sensitivity is %f LSB/g \n\r", 1.0f/aRes);
    // pc.printf("Gyroscope sensitivity is %f LSB/deg/s \n\r", 1.0f/gRes);
     // pc.printf("Magnetometer sensitivity is %f LSB/G \n\r", 1.0f/mRes);
      magbias[0] = +470.;  // User environmental x-axis correction in milliGauss, should be automatically calculated
      magbias[1] = +120.;  // User environmental x-axis correction in milliGauss
      magbias[2] = +125.;  // User environmental x-axis correction in milliGauss

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	  // If intPin goes high, all data registers have new data
	 	  if(readByte(MPU9250_ADDRESS, INT_STATUS) & 0x01) {  // On interrupt, check if data ready interrupt

	 	    readAccelData(accelCount);  // Read the x/y/z adc values
	 	    // Now we'll calculate the accleration value into actual g's
	 	    ax = (float)accelCount[0]*aRes - accelBias[0];  // get actual g value, this depends on scale being set
	 	    ay = (float)accelCount[1]*aRes - accelBias[1];
	 	    az = (float)accelCount[2]*aRes - accelBias[2];

	 	    readGyroData(gyroCount);  // Read the x/y/z adc values
	 	    // Calculate the gyro value into actual degrees per second
	 	    gx = (float)gyroCount[0]*gRes - gyroBias[0];  // get actual gyro value, this depends on scale being set
	 	    gy = (float)gyroCount[1]*gRes - gyroBias[1];
	 	    gz = (float)gyroCount[2]*gRes - gyroBias[2];

	 	    readMagData(magCount);  // Read the x/y/z adc values
	 	    // Calculate the magnetometer values in milliGauss
	 	    // Include factory calibration per data sheet and user environmental corrections
	 	    mx = (float)magCount[0]*mRes*magCalibration[0] - magbias[0];  // get actual magnetometer value, this depends on scale being set
	 	    my = (float)magCount[1]*mRes*magCalibration[1] - magbias[1];
	 	    mz = (float)magCount[2]*mRes*magCalibration[2] - magbias[2];
	 	  }

	 	 //   Now = t.read_us();
	 	 //   deltat = (float)((Now - lastUpdate)/1000000.0f) ; // set integration time by time elapsed since last filter update
	 	  //  lastUpdate = Now;

	 	   // sum += deltat;
	 	   // sumCount++;

	 	//    if(lastUpdate - firstUpdate > 10000000.0f) {
	 	//     beta = 0.04;  // decrease filter gain after stabilized
	 	//     zeta = 0.015; // increasey bias drift gain after stabilized
	 	 //   }

	 	   // Pass gyro rate as rad/s
	 	  MadgwickQuaternionUpdate(ax, ay, az, gx*PI/180.0f, gy*PI/180.0f, gz*PI/180.0f,  my,  mx, mz);
	 	 // mpu9250.MahonyQuaternionUpdate(ax, ay, az, gx*PI/180.0f, gy*PI/180.0f, gz*PI/180.0f, my, mx, mz);

	 	    // Serial print and/or display at 0.5 s rate independent of data rates
	 	   // delt_t = t.read_ms() - count;
	 	  //  if (delt_t > 500) { // update LCD once per half-second independent of read rate

	 	   /* pc.printf("ax = %f", 1000*ax);
	 	    pc.printf(" ay = %f", 1000*ay);
	 	    pc.printf(" az = %f  mg\n\r", 1000*az);

	 	    pc.printf("gx = %f", gx);
	 	    pc.printf(" gy = %f", gy);
	 	    pc.printf(" gz = %f  deg/s\n\r", gz);

	 	    pc.printf("gx = %f", mx);
	 	    pc.printf(" gy = %f", my);
	 	    pc.printf(" gz = %f  mG\n\r", mz);*/

	 	    tempCount = readTempData();  // Read the adc values
	 	    temperature = ((float) tempCount) / 333.87f + 21.0f; // Temperature in degrees Centigrade
	 	   // pc.printf(" temperature = %f  C\n\r", temperature);

	 	  //  pc.printf("q0 = %f\n\r", q[0]);
	 	  //  pc.printf("q1 = %f\n\r", q[1]);
	 	  //  pc.printf("q2 = %f\n\r", q[2]);
	 	 //   pc.printf("q3 = %f\n\r", q[3]);

	 	   // lcd.clear();
	 	   // lcd.printString("MPU9250", 0, 0);
	 	  //  lcd.printString("x   y   z", 0, 1);
	 	  //  lcd.setXYAddress(0, 2); lcd.printChar((char)(1000*ax));
	 	  //  lcd.setXYAddress(20, 2); lcd.printChar((char)(1000*ay));
	 	 //   lcd.setXYAddress(40, 2); lcd.printChar((char)(1000*az)); lcd.printString("mg", 66, 2);


	 	  // Define output variables from updated quaternion---these are Tait-Bryan angles, commonly used in aircraft orientation.
	 	  // In this coordinate system, the positive z-axis is down toward Earth.
	 	  // Yaw is the angle between Sensor x-axis and Earth magnetic North (or true North if corrected for local declination, looking down on the sensor positive yaw is counterclockwise.
	 	  // Pitch is angle between sensor x-axis and Earth ground plane, toward the Earth is positive, up toward the sky is negative.
	 	  // Roll is angle between sensor y-axis and Earth ground plane, y-axis up is positive roll.
	 	  // These arise from the definition of the homogeneous rotation matrix constructed from quaternions.
	 	  // Tait-Bryan angles as well as Euler angles are non-commutative; that is, the get the correct orientation the rotations must be
	 	  // applied in the correct order which for this configuration is yaw, pitch, and then roll.
	 	  // For more see http://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles which has additional links.
	 	    yaw   = atan2(2.0f * (q[1] * q[2] + q[0] * q[3]), q[0] * q[0] + q[1] * q[1] - q[2] * q[2] - q[3] * q[3]);
	 	    pitch = -asin(2.0f * (q[1] * q[3] - q[0] * q[2]));
	 	    roll  = atan2(2.0f * (q[0] * q[1] + q[2] * q[3]), q[0] * q[0] - q[1] * q[1] - q[2] * q[2] + q[3] * q[3]);
	 	    pitch *= 180.0f / PI;
	 	    yaw   *= 180.0f / PI;
	 	    yaw   -= 13.8f; // Declination at Danville, California is 13 degrees 48 minutes and 47 seconds on 2014-04-04
	 	    roll  *= 180.0f / PI;

	 	  //  pc.printf("Yaw, Pitch, Roll: %f %f %f\n\r", yaw, pitch, roll);
	 	  //  pc.printf("average rate = %f\n\r", (float) sumCount/sum);
	 	   uint8_t buf_size=sprintf((char*)test_buf,"Yaw, Pitch, Roll: %f %f %f\n\r", yaw, pitch, roll);
	 	   HAL_UART_Transmit_IT(&huart2, test_buf, buf_size);
	 	 //   myled= !myled;
	 	   // count = t.read_ms();
	 	    sum = 0;
	 	    sumCount = 0;





	 	    HAL_Delay(100); // częstotliwość ok. 10 Hz do testów

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSICalibrationValue = 0;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_6;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_MSI;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 40;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2|RCC_PERIPHCLK_I2C1;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure the main internal regulator output voltage
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x00702991;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Analogue filter
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Digital filter
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
